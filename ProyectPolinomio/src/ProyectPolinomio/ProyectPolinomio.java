/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProyectPolinomio;

import javax.swing.JOptionPane;

/**
 *
 * @author sala311
 */
public class ProyectPolinomio {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Polvf1 A, B;
        Polvf2 C;
        PolvLista D;

        int grado,gradon, gradoB, opcion,cantTerminos;
        float x;
       

        String menu = "**MENÚ** \n 1.Mostrar \n 2. Evaluar F1\n 3.Sumar F1\n 4.Multiplicar F1\n 5.Dividir con lista y f2, retorna f1 \n 6.Redimensionar\n 7.Dividir con f1 y f2, retorna lista\n 8.Dividir forma 1\n 9.Comparar\n  0.Salir  ";

        do {
            opcion = Integer.parseInt(JOptionPane.showInputDialog(menu));

            switch (opcion) {
                case 1:
                    grado = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el grado"));
                    A = new Polvf1(grado);
                    A.ingresarTerminos();
                    A.mostrar();
                    break;

                case 2:
                    grado = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el grado"));
                    A = new Polvf1(grado);
                    A.ingresarTerminos();
                    x = Float.parseFloat(JOptionPane.showInputDialog("Ingrese el valor de x a evaluar"));
                    float resultado = A.evaluarPolinomio(x);
                    JOptionPane.showMessageDialog(null, "Resultado: " + resultado);
                    break;

                case 3:                    
                    grado = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el grado"));
                    A = new Polvf1(grado);
                    A.ingresarTerminos();
                    
                    gradoB = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el grado de B"));
                    B = new Polvf1(gradoB);
                    B.ingresarTerminos();
                    
                    Polvf1 resultadosuma = A.sumar(B);
                    resultadosuma.mostrar();
                    break;

                case 4:
                    grado = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el grado"));
                    A = new Polvf1(grado);
                    A.ingresarTerminos();
                    
                    
                    gradoB = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el grado de B"));
                    B = new Polvf1(gradoB);
                    B.ingresarTerminos();
                    Polvf1 resultadoMultiplicar = A.multiplicar(B);
                    resultadoMultiplicar.mostrar();
                    break;

                case 5:
                    D = new PolvLista();
                    D.ingresarTerminos();
                    
                    cantTerminos = Integer.parseInt(JOptionPane.showInputDialog("Ingrese cantidad de terminos"));
                    C = new Polvf2(cantTerminos);                    
                    C.ingresarterminos(cantTerminos);
                    
                    Polvf1 resultadoDividirF1 = D.dividirRetornarForma1(C);
                    resultadoDividirF1.mostrar();
                    
                    break;
                case 6:                    
                    grado = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el grado"));
                    A = new Polvf1(grado);
                    A.ingresarTerminos();
                    
                    gradon = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el nuevo grado"));
                    A.redimensionar(gradon);
                    break;                    
                    
                case 7:       
                    grado = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el grado"));
                    A = new Polvf1(grado);
                    A.ingresarTerminos();                    
                    cantTerminos = Integer.parseInt(JOptionPane.showInputDialog("Ingrese cantidad de terminos"));
                    C = new Polvf2(cantTerminos);                    
                    C.ingresarterminos(cantTerminos);
                    PolvLista resultadoDividirReLista = A.dividirRetornaLista(C);
                    resultadoDividirReLista.mostrar();              
                  
                    break;     
                    
                case 8:  
                    grado = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el grado"));
                    A = new Polvf1(grado);
                    A.ingresarTerminos();
                    gradoB = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el grado de B"));
                    B = new Polvf1(gradoB);
                    B.ingresarTerminos();
                    Polvf1 resultadoDividir = A.dividir(B);
                    resultadoDividir.mostrar();                                   
                    break;
                    
                case 9:
                    grado = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el grado"));
                    A = new Polvf1(grado);
                    A.ingresarTerminos();
                    
                    cantTerminos = Integer.parseInt(JOptionPane.showInputDialog("Ingrese cantidad de terminos"));
                    C = new Polvf2(cantTerminos);                    
                    C.ingresarterminos(cantTerminos);
                    
                    A.comparar(C);
                    
                    break;                            
                    
                case 0:
                    System.exit(0);
                    break;
                    
                default:
                    JOptionPane.showMessageDialog(null, "Opción no válida.");

            }

        } while (opcion != 0);
    }

}
