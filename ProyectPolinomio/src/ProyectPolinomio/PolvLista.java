/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProyectPolinomio;

import javax.swing.JOptionPane;

/**
 *
 * @author sala311
 */
public class PolvLista {

    Nodo cab;

    public PolvLista() {
        cab = null;
    }
    // Metodo mostrar los terminos del polinomio

    public void mostrar() {

        Nodo q = cab;
        String salida = "<html>";

        if (cab == null) {
            JOptionPane.showMessageDialog(null, "El polinomio se encuentra vacio");
        } else {
            while (q != null) {
                if (q.getCoef() > 0 && q != cab) {
                    salida += "+";
                }                
                salida+=q.getCoef()+"X<sup>"+String.valueOf(q.getExp())+"</sup>";
                q = q.getLiga();
            }

        }

        salida += "</html>";
        JOptionPane.showMessageDialog(null, "Terminos del polinomio\n" + salida);
    }

    // METODO PARA INSERTAR TERMINO

    public void insertarTermino(float coef, int exp) {

        Nodo q = cab, ant = null, x = null;
        while (q != null && q.getExp() > exp) {

            ant = q;
            q = q.getLiga();
        }

        if (cab == null) {
            cab = new Nodo(coef, exp);
        } else if (q != null && q.getExp() == exp) {
            if (q.getCoef() + coef != 0) {
                q.setCoef(q.getCoef() + coef);
            }

            if (q == cab) {
                cab = cab.getLiga();
            } else {
                ant.setLiga(q.getLiga());
            }
            // delete(q) aqui se libera el nodo
        } else {
            x = new Nodo(coef, exp);
            x.setLiga(q);
            if (q == cab) {
                cab = x;
            } else {
                ant.setLiga(x);
            }
        }
    }

    public void ingresarTerminos() {

        int exp, pos;
        float coeficiente;
        String res;

        res = JOptionPane.showInputDialog("¿Desea ingresar un término? s/n");

        while (res.equals("s") == true) {
            coeficiente = Float.parseFloat(JOptionPane.showInputDialog("Ingrese coeficiente"));
            exp = Integer.parseInt(JOptionPane.showInputDialog("ingrese exponente"));
            this.insertarTermino(coeficiente, exp);

            res = JOptionPane.showInputDialog("¿Desea ingresar un término? s/n");
        }

    }
    
    public PolvLista copiar() {
        Nodo q = cab;
        PolvLista copia = new PolvLista();
        
        while (q != null) {
            copia.insertarTermino(q.getCoef(), q.getExp());
            q = q.getLiga();
        }
        return copia;
    }
    
    
     public Polvf1 dividirRetornarForma1(Polvf2 B) {
        
        float coet, coea, sumacoef;
        int expt, posa, post, expa, i;
        
        PolvLista copia = copiar();
        
        Nodo q = copia.cab;
              
        Polvf1 cociente = new Polvf1((int) q.getExp() - (int) B.getDato(1));
        
         while (q != null && (copia.cab.getExp() >= B.getDato(1))) {
            q = copia.cab;
            // dividr
            coet = copia.cab.getCoef() / B.getDato(2);
            expt = (int) copia.cab.getExp() - (int) B.getDato(1);
            cociente.insertarTerm(coet, expt);

            for (i = 1; i < B.getDato(0) * 2 + 1; i += 2) {
                coea = ((coet * B.getDato(i + 1)) * -1);
                expa = expt + ((int) B.getDato(i));
                
                while (q.getExp() != expa) {                
                    q  = q.getLiga();
                }
                
                sumacoef = q.getCoef() + coea;                   
          
                copia.insertarTerminoDividir(sumacoef, ((int) q.getExp()));                
                
            }
        }

        return cociente;
    }
     
     
       //Metodo insertar para dividir
    
    public void insertarTerminoDividir(float coef, int exp) {

      Nodo q=cab,ant=null,x;
      while(q != null && q.getExp() > exp)
      {
          ant=q;
          q=q.getLiga();
      }
      if(q != null  &&  q.getExp() == exp){
          if(coef!=0)
          {
              q.setCoef(coef);
          }
          else
          {
              if(q == cab)
              {
                  cab = cab.getLiga();
              }
              else
              {
                  ant.setLiga(q.getLiga());
              }
              //delete(q) aca se libera el nodo
          }
      }
      else
      {
         x=new Nodo(coef, exp);
         x.setLiga(q);
         if(q==cab)
         {
             cab=x;
         }
         else
         {
             ant.setLiga(x);
         }
     }
    }    
    
    
}
