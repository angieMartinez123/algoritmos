package ProyectPolinomio;

import javax.swing.JOptionPane;

public class Polvf2 {
     int n;
    float vec[];
    
//Metodo constructor
    public Polvf2(int canterm){
        n = canterm * 2 + 1;
        vec=new float[n];
        vec[0] = canterm;
    }

//Metodo para mostrar
    public void mostrar(){
        String salida="<html>";
        for(int k = 1;k < vec[0]*2+1;k += 2)
        {
            if(vec[k+1] > 0 && k > 2)
            {
                salida+="+";
            }
            
            //if (vec[k+1] != 0)
                salida+=(int)vec[k+1]+"X<sup>"+String.valueOf((int)vec[k])+"</sup>";
        }
           salida+="</html>";
         JOptionPane.showMessageDialog(null,"Terminos del polinomio\n"+salida);  
    }
 
//Metodo para insertar un dato en una posicion especifica
    public void setDato(float dato, int pos) {
        vec[pos] = dato;
    }
    
//Metodo para obtener un dato del vector con una posicion especifica
    public float getDato(int pos) {
        return vec[pos];
    }
    
    public float evaluar(float X){
        float resultado=0;
        int j=1;
        
        for (int i = 2; i < vec[0]*2+1; i+=2) {
            resultado += vec[i] * (Math.pow(X, vec[j]));
            j += 2;
        }
        
        return resultado;
    }
    
//Metodo para almacenar un termino en el polinomio
    public void almacenarTerm(float coef, int exp){
        int k=1,j;
        while((k < vec[0] * 2 + 1) && (vec[k] > exp) && (vec[k+1] != 0)){
           k+=2; 
        }
        if((k < vec[0] * 2 + 1) && (vec[k] == exp) && (vec[k+1] != 0)){
            JOptionPane.showMessageDialog(null,"Ya hay un termino con ese exponente");
        }
        else
        {
            for(j = (int)vec[0]*2 ;j > k+1; j--)
            {
               vec[j]=vec[j-2]; 
            }
            vec[k]=exp;
            vec[k+1]=coef;
        }
    }
      
//Metodo para insertar un termino en el polinomio
    public void insertarTerm(float coef, int exp){
        int k=1,j;
        while(k < vec[0]*2+1 && vec[k] > exp && vec[k+1] != 0)
        {
           k+=2; 
        }
        if((k < vec[0]*2+1)  &&  (vec[k] == exp) && (vec[k+1] != 0)){
            if(vec[k+1]+coef!=0){
               vec[k+1] += coef ;
            }
            else{
                for(j=k;j<vec[0]*2-1;j+=2)
                {
                   vec[j]=vec[j+2] ;
                   vec[j+1]=vec[j+3] ;
                }
                vec[0]=vec[0]-1;
            }
        }
        else
        {
            if(vec[0]*2+1 == n){
                this.redimensionar();
            }
            for(j=(int)vec[0]*2+1;j >= k;j--){
               vec[j+2]=vec[j]; 
            }
            vec[k]=exp;
            vec[k+1]=coef;
        }
    }

//Metodo para redimensionar
    public void redimensionar(){
        n=n+2;
        float aux[]=new float[n];
        for(int k = 0; k < vec[0]*2+1; k++){
          aux[k]=vec[k];
        }
        //Delete vec aqui se libera la memoria utilizada por vec
        vec=aux;
    }

//Metodo para ingresar los terminos del polinomio
    public void ingresarterminos(int canterm){
        float coef;
        int k, exp;
        for(k=1;k<=canterm;k++)
        {
            coef=Float.parseFloat(JOptionPane.showInputDialog("Ingrese el coeficiente"));
            exp=Integer.parseInt(JOptionPane.showInputDialog("Ingrese el exponente"));
            this.almacenarTerm(coef, exp);
        }
    }
    
//Metodo para sumar 2 polinomios
    public Polvf2 sumar(Polvf2 B){
        int k=1,j=1,expA,expB,coeA,coeB;
        Polvf2 R = new Polvf2((int)(vec[0]+B.getDato(0)));

        while ((k < vec[0]*2+1) && (j < B.getDato(0)*2+1)) {
            expA = (int)vec[k];
            expB = (int)B.getDato(j);
            coeA = (int)vec[k+1];
            coeB = (int)B.getDato(j+1);
            if (expA == expB) {
                if ((coeA + coeB) != 0) {
                    R.insertarTerm((coeA + coeB), expA);
                }
                k = k + 2;
                j = j + 2;
            }else{
                if (expA > expB) {
                    R.insertarTerm(coeA, expA);
                    k = k + 2;
                    
                }else{
                    R.insertarTerm(coeB, expB);
                    j = j + 2;
                }
            }
        }
        while (k < (vec[0]*2+1)) {            
            R.insertarTerm(vec[k+1], (int)vec[k]);
            k = k + 1;
        }
        while (j < (B.getDato(0)*2+1)) {
            R.insertarTerm(B.getDato(j+1), (int)B.getDato(j));
            j = j + 2;
        }

        return (R);
    }
    
//Metodo para Multiplicar 2 polinomios
    public Polvf2 multiplicar(Polvf2 B){
        int k,j,expA,expB,expR;
        float coeR;
        
        Polvf2 R = new Polvf2  ((int)(vec[0] + B.getDato(0)+1));
        
        R.setDato(vec[1] + B.getDato(1), 1);
        
        for (j = 1; j < (B.getDato(0)*2+1); j+=2) {
            expB = (int)B.getDato(j);
            for (k = 1; k < vec[0]*2+1; k+=2) {
                expA = (int)vec[k];
                expR = expA + expB;
                coeR = vec[k+1] * B.getDato(j+1);
                R.insertarTerm(coeR, expR);
            }
        }
        return (R);
    }
    

    public Polvf2 dividir(Polvf2 B){
        int expR,j=2;
        int i = 1;
        float coefR;
        Polvf2 R = null;
        
        if (vec[1] >= B.getDato(1)) {
            R = new Polvf2((int)((vec[0] - B.getDato(0)) * 2 + 1));
            while(i < vec[0]*2+1 && j < B.getDato(0)){
                expR = (int)(vec[i] - B.getDato(i));
                coefR = vec[j] / B.getDato(j);
                R.insertarTerm(coefR, expR);
                i+=2;
                j+=2;
            }
        }else{
            JOptionPane.showMessageDialog(null, "No se puede dividir");
        }
        
        return R;
    }
    
    public void comparar(Polvf2 B){
        boolean igual=false;
        if (vec[0] == B.getDato(0)) {
            for (int i = 1; i < vec[0]*2+1; i+=2) {
                if (vec[i] == B.getDato(i)  && vec[i+1] == B.getDato(i+1)) {
                    igual = true;
                }else{
                    igual = false;
                    i = (int)vec[0]*2+1;
                }
            }
            if (igual == true) {
               JOptionPane.showMessageDialog(null, "Los dos polinomios son iguales");
            }else{
                JOptionPane.showMessageDialog(null, "Los dos polinomios son diferentes");
            }
        }else{
            JOptionPane.showMessageDialog(null, "Los dos polinomios son diferentes");
        }
    }
}
