package ProyectPolinomio;

import javax.swing.JOptionPane;

/**
 *
 * @author sala311
 */
public class Polvf1 {

   int n;
    float vec[];

    // Método constructor
    public Polvf1(int grado) {
        this.n = grado + 2;
        this.vec = new float[n];
        this.vec[0] = grado;
    }

    public void setDato(float dato, int pos) {
        vec[pos] = dato;
    }

    public float getDato(int pos) {
        return vec[pos];
    }

    // Método para mostrar
    public void mostrar(){
        int i;
        String salida="<html>";
        for(i = 1;i < vec[0]+2;i++){
            if(vec[i] != 0){
                if (vec[i] > 0 && i > 1) {
                    salida+="+";
                }
                salida+=(int)vec[i]+"X<sup>"+String.valueOf((int)vec[0]+1-i)+"</sup>";
            }
        }
           salida+="</html>";
         JOptionPane.showMessageDialog(null,"Terminos del polinomio\n"+salida);  
    }

    // Llena el polinomio
    public void ingresarTerminos() {
        int exp, pos;
        float coef;
        String rpa;

        rpa = JOptionPane.showInputDialog("¿Desea ingresar un término? (S/N)");
        while (rpa.equals("S") || rpa.equals("s")) {
            coef = Float.parseFloat(JOptionPane.showInputDialog("Ingrese el coeficiente del término:"));
            exp = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el exponente del término:"));

            if (exp >= 0 && exp <= vec[0]) {
                pos = (int) (vec[0] + 1 - exp);
                if (vec[pos] == 0) {
                    vec[pos] = coef;
                } else {
                    JOptionPane.showMessageDialog(null, "Ya hay un termino con ese exponente");
                }
            } else {
                JOptionPane.showMessageDialog(null, "No corresponde al polinomio");
            }
            rpa = JOptionPane.showInputDialog("¿Desea ingresar un término? (S/N)");
        }
    }

    public float evaluar(float x) {
        float resultado = 0;

        for (int i = 1; i < vec[0] + 2; i++) {
            resultado += vec[i] * ((float) Math.pow(x, vec[0] + 1 - i));
        }
        return resultado;
    }

    public void redimensionar(int exp) {
        int k, pos = exp + 1;
        float aux[] = new float[exp + 2];

        for (k = (int) (vec[0] + 1); k > 0; k--) {
            aux[pos] = vec[k];
            pos--;
        }
        //delete vec aca se libera el vec
        vec = aux;
    }

    public void ajustar() {
        int k, j = 1, cont = 0;

        while ((j < (int) vec[0] + 2) && (vec[j] == 0)) {
            cont += 1;
            j += 1;
        }

        for (k = j; k < vec[0] + 2; k++) {
            vec[k - cont] = vec[k];
        }
        vec[0] = vec[0] - cont;
    }

    public void insertarTerm(float coef, int exp) {
        int pos;

        if (exp < 0) {
            JOptionPane.showMessageDialog(null, "El exponente no es valido");
        } else if (exp <= vec[0]) {
            pos = (int) vec[0] + 1 - exp;
            vec[pos] = vec[pos] + coef;
            this.ajustar();
        } else {
            this.redimensionar(exp);
            vec[0] = exp;
            vec[1] = coef;
        }
    }

    // método para evaluar el polinomio
    public float evaluarPolinomio(float x) {

        float suma = 0;
        for (int i = 1; i < vec[0] + 2; i++) {

            suma = suma + vec[i] * (float) Math.pow(x, (vec[0] + 1 - i));
        }
        return suma;
    }

    public Polvf1 sumar(Polvf1 B) {

        int k = 1, j = 1, expA = 0, expB = 0, gm, posR;

        if (vec[0] > B.getDato(0)) {
            gm = (int) vec[0];
        } else {
            gm = (int) B.getDato(0);
        }
        Polvf1 R = new Polvf1(gm);
        while ((k < vec[0] + 2) && (j < B.getDato(0) + 2)) {
            expA = (int) vec[0] + 1 - k;
            expB = (int) B.getDato(0) + 1 - j;

            if (expA == expB) {
                posR = (int) R.getDato(0) + 1 - expA;
                R.setDato(vec[k] + B.getDato(j), posR);

            } else {
                if (expA > expB) {
                    posR = (int) R.getDato(0) + 1 - expA;
                    R.setDato(vec[k], posR);
                } else {
                    posR = (int) R.getDato(0) + 1 - expB;
                    R.setDato(B.getDato(j), posR);
                }
            }
            k = k + 1;
            j = j + 1;
        }
        R.ajustar();
        return R;
    }

    public Polvf1 copiar() {

        int i = 1;
        Polvf1 copia = new Polvf1((int) vec[0]);
        for (i = 1; i < vec[0] + 2; i++) {
            copia.setDato(vec[i], i);
        }
        return copia;
    }

    public Polvf1 dividir(Polvf1 B) {

        float coet, coea, sumacoef;
        int expt, posa, post, expa, i,exptC;
        Polvf1 copia = copiar(); // copia de forma uno
        expt = (int) copia.getDato(0) - (int) B.getDato(0);
        Polvf1 cociente = new Polvf1(expt);

        while (copia.getDato(0) >= B.getDato(0)) {
            // dividr
            coet = copia.getDato(1) / B.getDato(1);
            exptC = (int) copia.getDato(0) - (int) B.getDato(0);
            cociente.insertarTerm(coet, exptC);

            for (i = 1; i < B.getDato(0) + 2; i++) {
                coea = ((coet * B.getDato(i)) * -1);
                expa = exptC + ((int) B.getDato(0) + 1 - i);
                sumacoef = copia.getDato(i) + coea;

                copia.setDato(sumacoef, ((int) copia.getDato(0) + 1) - expa);
            }
            // llamar el ajustar
            copia.ajustar();
        }
        return cociente;
    }

    public PolvLista dividirRetornaLista(Polvf2 B) {

        float coet, coea, sumacoef;
        int expt, expa, i,pos;
        Polvf1 copia = copiar(); // copia de forma uno
        PolvLista cociente = new PolvLista();

        while (copia.getDato(0) >= B.getDato(1)) {
            // dividr
            coet = copia.getDato(1) / B.getDato(2);
            expt = (int) copia.getDato(0) - (int) B.getDato(1);

            cociente.insertarTermino(coet, expt);

            for (i = 1; i < B.getDato(0) * 2 + 1; i+=2) {
                coea = ((coet * B.getDato(i + 1)) * -1);
                expa = expt + ((int) B.getDato(i));
                pos = ((int) copia.getDato(0) + 1) - expa;
                sumacoef = copia.getDato(pos) + coea;

                copia.setDato(sumacoef,pos) ;
            }

            // llamar el ajustar
            copia.ajustar();
        }

        return cociente;
    }

    public void comparar(Polvf2 B) {
        int i, j, expA, coefA, expB, coefB;
        boolean iguales = true;
        if(vec[0] != B.getDato(1) ){
            JOptionPane.showMessageDialog(null, "Los polinomios no son iguales");
        }else{
            for(i = 1; i < vec[0] + 2; i++){
                for(j = 1; j < B.getDato(0) * 2 + 1; j += 2){
                    expA = (int)vec[0] + 1 - i;
                    coefA = (int)vec[i];
                    expB = (int)B.getDato(j);
                    coefB = (int)B.getDato(j + 1);

                    if(expA != expB || coefA != coefB){
                        iguales = false;
                        break;
                    }
                }
            }
        }

        if(!iguales){
            JOptionPane.showMessageDialog(null, "Los polinomios no son iguales");
        }else{
            JOptionPane.showMessageDialog(null, "Los polinomios son iguales");
        }
    }

    
    public Polvf1 multiplicar(Polvf1 B){
        
        int k, j, grado, mExp;
        float mCoef;
        Polvf1 resultado = new Polvf1((int)vec[0] + (int)B.getDato(0));
        
        for(k = 1; k < vec[0] + 2; k++){
            for(j = 1; j < B.getDato(0) + 2; j++){
                mCoef = (int)vec[k] * (int)B.getDato(j);
                mExp = ((int)vec[0] + 1 - k) + ((int)B.getDato(0) + 1 - j);
                
                resultado.insertarTerm(mCoef, mExp);
            }
        }
        return resultado;
    }
}
