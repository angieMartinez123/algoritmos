/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProyectPolinomio;

import javax.swing.JOptionPane;

/**
 *
 * @author sala311
 */
public class PolvLista {
    
    Nodo cab;
    
    public PolvLista(){
        cab = null;
    }
    // Metodo mostrar los terminos del polinomio
    
    public void mostrar() {
        
        Nodo q = cab;
        String salida= "<html>";
        
        if(cab == null){
            JOptionPane.showMessageDialog(null, "El polinomi se encuentra vacio");
        }else {
            while (q!=null) {
                if(q.getCoef()>0 && q != cab){
                    salida+="+";
                }  
            
            salida+=q.getCoef() + "X<sup>"+String.valueOf(q.getExp()) + "</sup>";
                
            }
            
        }
        
        salida+="</html>";
        JOptionPane.showMessageDialog(null, "Terminos del polinomio " + salida);
    }
    
    //METODO PARA INSERTAR TERMINO
    
    public void insertarTermino(float coef,int exp){
        
        Nodo q = cab,ant = null,x= null;
        while(q!=null&&q.getExp()>exp){
            
            ant=q;
            q=q.getLiga();            
        }
        
        if(q!=null && q.getExp()==exp){
            if(q.getCoef()+coef!=0){
                q.setCoef(q.getCoef()+coef);                
            }
            
                        if(q==cab){
                cab = cab.getLiga();
            }else {
                ant.setLiga(q.getLiga());
            }
                        //delete(q) aqui se libera el nodo
        }else {
            x= new Nodo(coef, exp, null);
            x.setLiga(q);
            if(q==cab){
                cab = x;
            } else {                
                ant.setLiga(x);
            }
        }
    }
    
    public void ingresarTerminos(){
        
        int exp, pos;
        float coeficiente;
        String res;   
        
        res = JOptionPane.showInputDialog("¿Desea ingresar un término? s/n");
        
        while (res.equals("s")==true) {
           coeficiente = Float.parseFloat(JOptionPane.showInputDialog("Ingrese coeficiente"));
           exp = Integer.parseInt(JOptionPane.showInputDialog("ingrese exponente"));
           this.insertarTermino(coeficiente, exp);      

           res = JOptionPane.showInputDialog("¿Desea ingresar un término? s/n");
        }
        
    }    
}
