/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ProyectPolinomio;

import javax.swing.JOptionPane;

/**
 *
 * @author sala311
 */
public class ProyectPolinomio {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Polvf1 A, B;

        int grado,gradon, gradoB, opcion;
        float x;
       
        grado = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el grado"));
        A = new Polvf1(grado);
        A.ingresarTerminos();

        String menu = "**MENÚ** \n 1.Mostrar \n 2. Evaluar \n 3.Sumar \n 4.Multiplicar\n 5.Dividir\n 0.Salir\n 6.Redimensionar\n 7.Sumar";

        do {
            opcion = Integer.parseInt(JOptionPane.showInputDialog(menu));

            switch (opcion) {
                case 1:
                    A.mostrar();
                    break;

                case 2:
                    x = Float.parseFloat(JOptionPane.showInputDialog("Ingrese el valor de x a evaluar"));
                    float resultado = A.evaluarPolinomio(x);
                    JOptionPane.showMessageDialog(null, "Resultado: " + resultado);
                    break;

                case 3:
                    break;

                case 4:
                    break;

                case 5:
                    break;

                case 0:
                    System.exit(0);
                    break;
                    
                case 6:
                    
                    gradon = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el nuevo grado"));
                    A.redimencionar(grado);
                    break;
                    
                case 7:
                    
                    gradoB = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el grado de B"));
                    B = new Polvf1(grado);
                    B.ingresarTerminos();
                    Polvf1 resultadosuma = A.sumar(B);
                    resultadosuma.mostrar();
                    break;                    

                default:
                    JOptionPane.showMessageDialog(null, "Opción no válida.");

            }

        } while (opcion != 0);
    }

}
