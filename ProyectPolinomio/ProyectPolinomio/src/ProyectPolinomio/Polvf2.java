package ProyectPolinomio;

import javax.swing.JOptionPane;

public class Polvf2 {
    
    int n;
    float vec[];
    
    public Polvf2(int canTer) {    
        n = canTer*2+1;
        vec=new float[n];
        vec[0] = canTer;
    }
    
    public void mostrar(){
        
        String salida= "<html>";
        
        for (int i = 1;i<vec[0]*2+1; i+=2){
            if (vec[i+1]>0&&i>2) {
                salida+="+";
            }
            salida+=vec[i+1] + "X<sup>"+String.valueOf(vec[i]) + "</sup>";
        }
        salida+="</html>";
        JOptionPane.showMessageDialog(null, "Terminos del polinomio " + salida);
    }
    
    public void almacenarTerminos(float coef, int exp){
        
        int k = 1,j;
        
        while ((k < vec[0]*2+1) && (vec[k]>exp) && (vec[k+1]!=0)) {
            k+=2;            
        }
        
        //if(){}
        
        if ((k < vec[0]*2+1) && (vec[k] == exp) && (vec[k+1]!=0)) {
            if(vec[k+1] + coef != 0){
                vec[k+1] += coef;
            }else {
                for (j = k; j < vec[0]*2-1; j++) {
                    vec[j] = vec[j+2];
                    vec[j+1] = vec[j+3];
                }
                vec[0]=vec[0]-1;
            }
        }else {
            if(vec[0]*+1==n){
                //this.redimensionar();
            }
            
            for(j=(int)vec[0]*2;j>=k;j--){
                vec[j+2]=vec[j];
            }
            
            vec[k] = exp;
            vec[k+1] = coef;
        }
        
    }
    
    public void redimensionar(){
        
        n=n+2;
        float aux[] = new float[n];
        
        for (int i = 0; i < vec[0]*2+1; i++) {
            aux[i] = vec[i];
        }
        //delete vec aqui se libera en memoria utiliza por el vec
        
        vec=aux;
    }
    
    //metodo para ingresar los terminos del polinomio
    
    public void ingrsarTerminos(int cantTerminos) {
        
        float coef;
        int k, exp;
        
        for (k = 1; k <= cantTerminos; k++) {
            coef = Float.parseFloat(JOptionPane.showInputDialog("Ingrese el coeficiente"));
            exp= Integer.parseInt(JOptionPane.showInputDialog("Ingrese el exponente"));
            this.almacenarTerminos(coef, exp);
        }
        
    }
}
