package ProyectPolinomio;

import javax.swing.JOptionPane;

/**
 *
 * @author sala311
 */
public class Polvf1 {
    
    int n;
    float vec[];
    
    //Método constructor
    public Polvf1(int grado) {
        
        n = grado+2;
        vec = new float[n];
        
        //asigna grado en la posicion 0
        vec[0] = grado;
    }  
    
    //método para mostrar
    
    public void mostrar() {
        
        int k;
        String salida = "";
        
        for (k = 1; k < vec[0] + 2; k++) {
            if(vec[k] != 0){
                if((vec[k]>0) && (k > 1)) {
                       salida = salida + " + " + vec[k] + "X ^" + (vec[0] + 1 - k);
                }else {
                    salida = salida + vec[k] + "X ^" + (vec[0] + 1 - k);
                }
            }
        }
        
        JOptionPane.showMessageDialog(null, "Resultado del vector" + salida);        
    }
    
    //método para evaluar el polinomio
    
    public float evaluarPolinomio(float x){
        
        float suma = 0;        
        for (int i = 1; i < vec[0] + 2; i++) {
            
            suma = suma + vec[i] * (float) Math.pow(x, (vec[0] + 1 -i));
        }
        return suma;
    }
    
    public float getDato(int pos){
        return (vec[pos]);
    }
    
    public void setDato(float dato, int pos){
        vec[pos] = dato;
    }    
    //método para redimencionar el vector
    public void redimencionar(int exp){
        
        int k, pos = exp +1;
        float aux[] = new float[exp + 2];
        
        for(k = (int)vec[0]+1; k>0; k--){
            aux[pos] = vec[k];
            pos --;
        }
    }
    
    public void ingresarTerminos(){
        
        int exp, pos;
        float coeficiente;
        String res;   
        
        res = JOptionPane.showInputDialog("¿Desea ingresar un término? s/n");
        
        while (res.equals("s")==true) {
           coeficiente = Float.parseFloat(JOptionPane.showInputDialog("Ingrese coeficiente"));
           exp = Integer.parseInt(JOptionPane.showInputDialog("ingrese exponente"));
           
           //almacenar
            if ((exp >= 0) && (exp <= vec[0])) {

                pos = (int)vec[0]+1-exp;

                if(vec[pos] == 0){
                    vec[pos] = coeficiente;
                }else {
                    JOptionPane.showMessageDialog(null, "Se encuentra un termino con ese exponente");
                }                               
            }else{
                 JOptionPane.showMessageDialog(null, "El exponente no corresponde al polinomio");
            }

           res = JOptionPane.showInputDialog("¿Desea ingresar un término? s/n");
        }
        
    }
    
    public void redimensionar(int exp){
        
        int k, pos = exp +1;
        float aux[] = new float[exp + 2];
        
        for(k= (int)vec[0] + 1; k> 0; k--){
            aux[pos] = vec[k];
            pos --;
        }
        //delete se elimina las pocisiones del eliminar
        vec = aux;
    }
    
    public void ajustar(){
        int k, j=0, cont = 0;
        
        while((j < (int)vec[0] + 2) &&(vec[j] == 0)){
            cont = cont +1;
            j = j+1;
            
        }
        
        for (k = j; k < vec[0]+2 ; k++){
            vec[k-cont] = vec[k];
        }
        
        vec[0] = vec[0] - cont;
    }
    
    public void insertarTerm(float coef, int exp){
        int pos;
        if (exp > 0){
            JOptionPane.showMessageDialog(null, "El exponente no es válido");
        }else{
            if(exp <= vec[0]){
                pos = (int)vec[0] + (int)coef;
                ajustar();
                
            }else{
                this.redimencionar(exp);
                vec[0] = exp;
                vec[1] = coef;
            }
        }
    }
    
    public Polvf1 sumar (Polvf1 B){
        
        int k = 1, j = 1, expA = 0, expB = 0, gm, posR;
        
        if(vec[0] > B.getDato(0)){
            gm = (int)vec[0];
        }else{
            gm = (int)B.getDato(0);
        }        
        Polvf1 R = new Polvf1(gm);
        while((k < vec[0]+2) && (j < B.getDato(0)+2)){
            expA = (int)vec[0]+1 - k;
            expB = (int)B.getDato(0) + 1 - j;

            if(expA == expB){
                posR = (int)R.getDato(0)+1 - expA;
                R.setDato(vec[k]+B.getDato(j), posR);

            }else{
                if(expA > expB){
                    posR = (int)R.getDato(0)+1 - expA;
                    R.setDato(vec[k], posR);
                }else{
                    posR = (int)R.getDato(0) + 1 - expB;
                    R.setDato(B.getDato(j), posR);
                }
            }
            k = k +1;
            j = j +1;
        }
        R.ajustar();
        return R;
    }
    
}
